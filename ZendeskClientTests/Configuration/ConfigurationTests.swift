//
//  ConfigurationTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class ConfigurationTests: XCTestCase {

    func testBaseUrl() {
        XCTAssertTrue(AppConfig.baseUrl == "https://mxtechtest.zendesk.com/api/v2/")
    }
}
