//
//  TicketListInteractor.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Foundation

protocol TicketListInteractor {
    /// Fetches the Agent Tickets
    ///
    /// - Parameter completion: completion block to notify caller with either tickets array or an error.
    func fetchTickets(_ completion: @escaping (Result<[Ticket]>) -> Void)
}
