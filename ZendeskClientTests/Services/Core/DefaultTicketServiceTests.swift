//
//  DefaultTicketServiceTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class DefaultTicketServiceTests: XCTestCase {
    private var networkingManagerMock: NetworkingManagerMock?
    private var ticketService: DefaultTicketService?
    override func setUp() {
        networkingManagerMock = NetworkingManagerMock()
        ticketService = DefaultTicketService(networkingManager: networkingManagerMock!, baseUrl: "http://google.com")
    }

    func testHappyPath() {
        networkingManagerMock?.fetchSuccessResponse = try! JSONReader.jsonData(fileName: "TicketsList")
        ticketService?.fetchTicket(userName: "ismail", password: "pass", sortBy: .subject, ascending: false, { (result) in
            switch result {
            case .success(let ticketResponse): XCTAssertTrue(ticketResponse.tickets.count == 45)
            case .failure(_): XCTAssert(false)
            }
        })
        
        XCTAssertTrue(networkingManagerMock!.performedRequest!.url!.absoluteString == "http://google.com/views/39551161/tickets.json?sort_by=subject&sort_order=desc")
        XCTAssertTrue(networkingManagerMock!.performedRequest!.allHTTPHeaderFields!["Authorization"] == "Basic aXNtYWlsOnBhc3M=")
        XCTAssertTrue(networkingManagerMock!.calledFunctions.contains("perform(request:_:)"))
    }
    
    func testInvalidJSONErrorPath() {
        networkingManagerMock?.fetchSuccessResponse = try! JSONReader.jsonData(fileName: "FaultyTicketsList")
        ticketService?.fetchTicket(userName: "ismail", password: "pass", sortBy: .subject, ascending: false, { (result) in
            switch result {
            case .success(_): XCTAssert(false)
            case .failure(let error):
                XCTAssertTrue((error as NSError) == NSError.invalidJSONError())
            }
        })
        
        XCTAssertTrue(networkingManagerMock!.performedRequest!.url!.absoluteString == "http://google.com/views/39551161/tickets.json?sort_by=subject&sort_order=desc")
        XCTAssertTrue(networkingManagerMock!.performedRequest!.allHTTPHeaderFields!["Authorization"] == "Basic aXNtYWlsOnBhc3M=")
        XCTAssertTrue(networkingManagerMock!.calledFunctions.contains("perform(request:_:)"))

    }
}

private final class NetworkingManagerMock: BaseMock, NetworkingManager {
    var performedRequest: URLRequest?
    var fetchSuccessResponse: Data?
    func perform(request: URLRequest, _ completion: @escaping (Result<Data>) -> Void) {
        calledFunctions.append(#function)
        performedRequest = request
        if let responseData = fetchSuccessResponse {
            completion(Result.success(responseData))
        }
        else {
            completion(Result.failure(NSError(domain: "TestDomain", code: -100, userInfo: nil)))
        }
    }
}
