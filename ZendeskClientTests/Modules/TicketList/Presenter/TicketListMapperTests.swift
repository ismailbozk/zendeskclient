//
//  TicketListMapperTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class TicketListMapperTests: XCTestCase {
    var mapper: TicketListMapper!
    
    override func setUp() {
        mapper = TicketListMapper()
    }

    func testMapping() {
        let displayModels = mapper.mapTicketsToTicketDisplay(tickets: tickets())
        let subjectTitle = NSLocalizedString("SUBJECT_TITLE", comment: "ticket recoerd subject title.")
        
        XCTAssertTrue(displayModels.count == 2)
        XCTAssertTrue(displayModels.first!.id == "#0")
        XCTAssertTrue(displayModels.last!.id == "#1")
        XCTAssertTrue(displayModels.first!.subject == String(format: subjectTitle, "Something happened"))
        XCTAssertTrue(displayModels.last!.subject == String(format: subjectTitle, "Something happened again"))
        XCTAssertTrue(displayModels.first!.status == "Pending")
        XCTAssertTrue(displayModels.last!.status == "New")
        XCTAssertTrue(displayModels.first!.description == "something happened and I don't know what to do")
        XCTAssertTrue(displayModels.last!.description == "something happened and I don't know what to do again")
    }
    
    private func tickets() -> [Ticket] {
        return [
            Ticket(id: 0, subject: "Something happened", description: "something happened and I don't know what to do", status: .pending),
            Ticket(id: 1, subject: "Something happened again", description: "something happened and I don't know what to do again", status: .new),
        ]
    }
}
