//
//  TicketListBuilder.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import UIKit

class TicketListBuilder {
    /// Builds Ticket List VIPER Stack and returns the ViewController which holds the rest of the VIPER comnponents
    ///
    /// - Returns: Ticket List View Controller
    func buildTicketListModule() -> UIViewController {
        let networkingManager = AlamofireNetworking()
        let service = DefaultTicketService(networkingManager: networkingManager, baseUrl: AppConfig.baseUrl)
        let interactor = DefaultTicketListInteractor(ticketService: service)
        let view = ticketListviewController()
        let presenter = DefaultTicketListPresenter(view: view,
                                                   interactor: interactor,
                                                   mapper: TicketListMapper())
        view.eventHandler = presenter
        return view
    }
    
    private func ticketListviewController() -> TicketListViewController {
        let storyboard = UIStoryboard(name: "Main", bundle: Bundle.main)
        return storyboard.instantiateInitialViewController() as! TicketListViewController
    }
}
