//
//  TicketListViewControllerTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class TicketListViewControllerTests: XCTestCase {
    private var eventHandlerMock: TicketListEventHandlerMock!
    private var viewController: TicketListViewController!
    
    override func setUp() {
        eventHandlerMock = TicketListEventHandlerMock()
        viewController = (UIStoryboard(name: "Main", bundle: Bundle.main).instantiateInitialViewController() as! TicketListViewController)
        viewController.eventHandler = eventHandlerMock
    }

    func testLifeCycle() {
        viewController.viewDidAppear(false)
        
        XCTAssertNotNil(viewController.eventHandler)
        XCTAssertTrue(eventHandlerMock.calledFunctions.contains("viewDidAppear()"))
    }
}


private final class TicketListEventHandlerMock: BaseMock, TicketListEventHandler {
    func viewDidAppear() {
        calledFunctions.append(#function)
    }
    
    func retryTapped() {
        calledFunctions.append(#function)
    }
}
