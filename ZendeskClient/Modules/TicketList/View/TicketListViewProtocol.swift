//
//  TicketListViewProtocol.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Foundation

protocol TicketListViewProtocol: AnyObject {
    /// Display given ticket on the view
    ///
    /// - Parameter tickets: ticket displays
    func display(tickets: [TicketDisplay])
    /// Starts loading animation
    func startLoading()
    /// Stops loading animation
    func dismissLoading()
    /// Displays given error to user
    func display(error: String)
}
