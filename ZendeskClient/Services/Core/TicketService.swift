//
//  TicketService.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Foundation

/// Ticket Fetch api call sort options
///
/// - id: sort by ticket id option
/// - status: sort by ticket status option
/// - subject: sort by ticket subject option
enum TicketSortByOption {
    /// Sort by ticket id option
    case id
    /// Sort by ticket status option
    case status
    /// Sort by ticket subject option
    case subject
}

/// Ticket Service Api calls.
protocol TicketService {
    /// related to the provided agent with the sorting and ascending options.
    ///
    /// - Parameters:
    ///   - userName: Agent's account user name.
    ///   - password: Agent's account password.
    ///   - sortBy: Records sort option. Please refer to TicketSortByOption.
    ///   - ascending: Ascending/Descending option of returning records.
    ///   - completion: Network call completion closure, will contain error or the response data.
    func fetchTicket(userName: String, password: String, sortBy: TicketSortByOption?, ascending: Bool?, _ completion: @escaping (Result<TicketListResponse>) -> Void)
}
