//
//  NetworkingManager.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Foundation

/// Networking Response encapsulator enum for both error or success casses
///
/// - success: Network call success case
/// - failure: Network call failure case
enum Result<Response> {
    case success(Response)
    case failure(Error)
    
    /// Success case initializer
    ///
    /// - Parameter value: Any response object.
    public init(value: Response) {
        self = .success(value)
    }
    
    /// Failure case initializer
    ///
    /// - Parameter error: Any Swift.Error object.
    public init(error: Error) {
        self = .failure(error)
    }
}

/// Networking Manager interface for Zendesk Client.
protocol NetworkingManager {
    /// Performs given request
    ///
    /// - Parameters:
    ///   - request: URLRequest
    ///   - completion: completion of Network request
    func perform(request: URLRequest, _ completion: @escaping (Result<Data>) -> Void)
}
