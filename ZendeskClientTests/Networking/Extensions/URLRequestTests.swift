//
//  URLRequestTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class URLRequestTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    func testAddBasicAuthHeader() {
        let url = URL(string: "http://kinggizzardandthelizardwizard.com")!
        var urlRequest = URLRequest(url: url)
        urlRequest.addValue("Jaguar Pau Ferro", forHTTPHeaderField: "Guitar")
        urlRequest.addBasicAuthCredentials(userName: "KingGizzard", password: "PopMusicIsTheWorst,60'sRock")
        
        XCTAssertNotNil(urlRequest.allHTTPHeaderFields)
        let headers = urlRequest.allHTTPHeaderFields!
        XCTAssertTrue(headers.count == 2)
        XCTAssertTrue(headers["Guitar"] == "Jaguar Pau Ferro")
        XCTAssertTrue(headers["Authorization"] == "Basic S2luZ0dpenphcmQ6UG9wTXVzaWNJc1RoZVdvcnN0LDYwJ3NSb2Nr")
    }
}
