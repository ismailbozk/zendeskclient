//
//  TicketDisplay.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//  Copyright © 2018 iBozkurt. All rights reserved.
//

import UIKit

struct TicketDisplay {
    let id: String
    let subject: String
    let status: String
    let statusColor: UIColor
    let description: String
}
