//
//  ErrorTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class ErrorTests: XCTestCase {
    func testGenericInvalidJSONError() {
        let invalidJSONDescription = NSLocalizedString("INVALID_JSON_ERROR", comment: "Some")
        let invalidJSONError = NSError.invalidJSONError()
        XCTAssertTrue(invalidJSONError.localizedDescription == invalidJSONDescription)
        XCTAssertTrue(invalidJSONError.code == -666)
        XCTAssertTrue(invalidJSONError.domain == "com.ibozkurt.json.parsing")
    }
}
