//
//  TicketCell.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import UIKit

class TicketCell: UITableViewCell {
    @IBOutlet private weak var idLabel: UILabel!
    @IBOutlet private weak var subjectLabel: UILabel!
    @IBOutlet private weak var statusLabel: UILabel!
    @IBOutlet private weak var descriptionLabel: UILabel!
    
    /// Updates the content of the cell with the given Ticket display model.
    func updateContent(with ticketDisplay: TicketDisplay) {
        idLabel.text                = ticketDisplay.id
        subjectLabel.text           = ticketDisplay.subject
        statusLabel.text            = ticketDisplay.status
        statusLabel.textColor       = ticketDisplay.statusColor
        descriptionLabel.text       = ticketDisplay.description
    }
}
