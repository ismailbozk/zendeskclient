//
//  TicketListMapper.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//  Copyright © 2018 iBozkurt. All rights reserved.
//

import UIKit

class TicketListMapper {
    /// Maps Ticket entities to ticket display models
    func mapTicketsToTicketDisplay(tickets: [Ticket]) -> [TicketDisplay] {
        let subjectTitle = NSLocalizedString("SUBJECT_TITLE", comment: "Subject title of the ticket")
        return tickets.map { ticket in
            let statusColor: UIColor
            switch ticket.status {
            case .new: statusColor = UIColor.blue
            case .pending: statusColor = UIColor.orange
            case .statusOpen: statusColor = UIColor.green
            }
            
            let subject = String(format: subjectTitle, ticket.subject)
            return TicketDisplay(id: "#\(ticket.id)",
                subject: subject,
                status: ticket.status.rawValue.capitalized,
                statusColor: statusColor,
                description: ticket.description)
        }
    }
}
