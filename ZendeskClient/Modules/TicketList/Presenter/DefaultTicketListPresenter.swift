//
//  TicketListPresenter.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//  Copyright © 2018 iBozkurt. All rights reserved.
//

import UIKit

/// Ticket List Presenter that cooridinates the Ticket List module
class DefaultTicketListPresenter {
    private weak var view: TicketListViewProtocol?
    private let interactor: TicketListInteractor
    private let mapper: TicketListMapper
    
    init(view: TicketListViewProtocol,
         interactor: TicketListInteractor,
         mapper: TicketListMapper) {
        self.view = view
        self.interactor = interactor
        self.mapper = mapper
    }
    
    /// Fetches the Zendesk Agent's tickets.
    private func fetchTickets() {
        view?.startLoading()
        interactor.fetchTickets { [weak self] (result) in
            guard let strongSelf = self,
                let view = strongSelf.view
                else { return }
            
            view.dismissLoading()
            
            switch result {
            case .success(let tickets):
                let ticketDisplayList = strongSelf.mapper.mapTicketsToTicketDisplay(tickets: tickets)
                view.display(tickets: ticketDisplayList)
            case .failure(let error):
                view.display(error: error.localizedDescription)
            }
        }
    }
}

extension DefaultTicketListPresenter: TicketListEventHandler {
    func viewDidAppear() {
        fetchTickets()
    }
    
    func retryTapped() {
        fetchTickets()
    }    
}
