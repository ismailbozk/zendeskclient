//
//  TicketListEventHandler.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Foundation

/// The Events Emitted by View for Event handler to handle.
protocol TicketListEventHandler {
    /// Triggered when the view is visible to user.
    func viewDidAppear()
    /// Triggered when user taps on retry button.
    func retryTapped()
}
