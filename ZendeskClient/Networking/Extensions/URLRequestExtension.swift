//
//  URLRequestExtension.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//  Copyright © 2018 iBozkurt. All rights reserved.
//

import Foundation
import Alamofire

extension URLRequest {
    /// URLRequest helper function to add Basic Auth Credentials to HTTP headers.
    ///
    /// - Parameters:
    ///   - userName: user name or user email address of user
    ///   - password: password of the user's Zendesk account
    ///
    /// **Note:** refer to [Wikipedia](https://en.wikipedia.org/wiki/Basic_access_authentication#URL_encoding) for furher information.
    mutating func addBasicAuthCredentials(userName: String, password: String) {
        guard let authorizationHeader = Request.authorizationHeader(user: userName, password: password) else { return }
        
        var allHTTPHeaderFields = self.allHTTPHeaderFields ?? [:]
        allHTTPHeaderFields[authorizationHeader.key] = authorizationHeader.value
        self.allHTTPHeaderFields = allHTTPHeaderFields
    }
}
