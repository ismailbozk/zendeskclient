//
//  AppConfig.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 21/10/2018.
//  Copyright © 2018 iBozkurt. All rights reserved.
//

import Foundation

fileprivate enum ConfigOption: String {
    case baseUrl = "BASE_URL"
}

class AppConfig {
    static var baseUrl: String { return AppConfig.infoForKey(.baseUrl) }
    
    private class func infoForKey(_ key: ConfigOption) -> String {
        return (Bundle.main.infoDictionary?[key.rawValue] as? String)!.replacingOccurrences(of: "\\", with: "")
    }
}
