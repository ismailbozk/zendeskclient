//
//  DefaultTicketService.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Foundation

/// Service which contains all ticket related api calls.
class DefaultTicketService {
    private let baseUrl: String
    private let networkingManager: NetworkingManager
    
    /// DefaultTicketService
    ///
    /// - Parameters:
    ///   - networkingManager: networking manager which will be the gate way for the network calls.
    ///   - baseUrl: the base url that the tickets will be fetched.
    init(networkingManager: NetworkingManager, baseUrl: String) {
        self.networkingManager = networkingManager
        self.baseUrl = baseUrl
    }
    
    // MARK: - Constants -
    private let viewPath = "/views"
    private let ticketPath = "/tickets.json"
    private let viewID = "39551161"

    // MARK: - Sort By url query key and values
    private let sortOrderQueryKey = "sort_order"
    private let sortOrderAscending = "asc"
    private let sortOrderDescending = "desc"

    // MARK: - Sort By url query key and values
    private let sortByQueryKey = "sort_by"
    private let sortBySubject = "subject"
    private let sortByID = "id"
    private let sortByStatus = "status"

    
    // MARK: - Private Url Helpers
    private func fetchTicketUrl(sortBy: TicketSortByOption?, ascending: Bool?) -> URL {
        guard let baseUrl = URL(string: baseUrl),
            var urlComponents = URLComponents(url: baseUrl, resolvingAgainstBaseURL: true)
            else {
            fatalError("Ticket Base Url is invalid")
        }
        urlComponents.path += viewPath
        urlComponents.path += "/\(viewID)"
        urlComponents.path += ticketPath
        
        urlComponents.queryItems = [sortByQueryItem(for: sortBy),sortOrderQueryItem(ascending: ascending)]
            .compactMap { $0 }
        
        guard let url = urlComponents.url else {
            fatalError("ticket url couldn't be established")
        }
        return url
    }

    private func sortByQueryItem(for sortBy: TicketSortByOption?) -> URLQueryItem? {
        guard let sortBy = sortBy else { return nil }
        
        let sortByValue: String
        switch sortBy {
        case .id:       sortByValue = sortByID
        case .subject:  sortByValue = sortBySubject
        case .status:   sortByValue = sortByStatus
        }
        
        return URLQueryItem(name: sortByQueryKey, value: sortByValue)
    }

    private func sortOrderQueryItem(ascending: Bool?) -> URLQueryItem? {
        guard let ascending = ascending else { return nil }
        
        let sortOrderValue = ascending ? sortOrderAscending : sortOrderDescending
        return URLQueryItem(name: sortOrderQueryKey, value: sortOrderValue)
    }
    
    // MARK: - Private Response Handlers
    private func handleSuccess<T>(data: Data) -> Result<T> where T: Decodable {
        do {
            let response = try JSONDecoder().decode(T.self, from: data)
            return Result<T>(value: response)
        }
        catch _ {
            return Result<T>(error: NSError.invalidJSONError())
        }
    }
}

extension DefaultTicketService: TicketService {
    /// Fetches tickets related to the provided agent with the sorting and ascending options.
    ///
    /// - Parameters:
    ///   - userName: Agent's Zendesk account user name
    ///   - password: Agent's Zendesk account password
    ///   - sortBy: Records sort option. Please refer to TicketSortByOption
    ///   - ascending: Ascending/Descending option of returning records
    ///   - completion: Network call completion closure, will contain error or the response data
    ///
    /// **Note:** All service documention can be found [here](https://developer.zendesk.com/rest_api/docs/core/views#list-tickets-from-a-view)
    func fetchTicket(userName: String, password: String, sortBy: TicketSortByOption?, ascending: Bool?, _ completion: @escaping (Result<TicketListResponse>) -> Void) {
        var request = URLRequest(url: fetchTicketUrl(sortBy: sortBy, ascending: ascending))
        request.addBasicAuthCredentials(userName: userName, password: password)

        networkingManager.perform(request: request) { [weak self] (response) in
            guard let strongSelf = self else { return }
            switch response {
            case .success(let data):  completion(strongSelf.handleSuccess(data: data) as Result<TicketListResponse>)
            case .failure(let error): completion(.failure(error))
            }
        }
    }
}
