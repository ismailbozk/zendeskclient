# ZendeskClient

Zendesk client app to monitor agent service tickets.

## Requirements

- iOS 12.0
- Xcode 10
- Swift 4.2
- Carthage 0.31.1

## Frameworks

- [Alamofire](https://github.com/Alamofire/Alamofire) 4.7.3

## Installation

No installation step required, just download the repository and run. Though unit tests can run only on **Development Zendesk Client** scheme.

## Project structure and Design Patterns

There are 2 schemes defined under the project;

- **Zendesk Client** for Release
- **Development Zendesk Client** for debugging and unit testing.

In order to achieve the most possible clean code, a variant of [VIPER](https://www.objc.io/issues/13-architecture/viper/) implemented.
Also project code base covered with unit and UI tests.


## Technical Debts
Removing the user authentication credentials from the code is the most urgent task. Implementing a proper login and if necessary, storing user username and password on keychain are the best practices for users' security.
