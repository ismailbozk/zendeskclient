//
//  DefaultTicketListInteractor.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Foundation

class DefaultTicketListInteractor {
    private let service: TicketService
    init(ticketService: TicketService) {
        self.service = ticketService
    }
    
    // MARK: - Constants
    private let agentUserName = "acooke+techtest@zendesk.com"
    private let agentPassword = "mobile"
    private let defaultSortByOption = TicketSortByOption.id
    private let defaultSortAscendingOrder = true
}

extension DefaultTicketListInteractor: TicketListInteractor {
    /// Fetches tickets related to the Zendesk Agent.
    ///
    /// - Parameter completion: completion that returns ticket list or an error.
    ///
    /// **Note:** the tickets are sorted by ascending _id_ order
    func fetchTickets(_ completion: @escaping (Result<[Ticket]>) -> Void) {
        service.fetchTicket(userName: agentUserName,
                            password: agentPassword,
                            sortBy: defaultSortByOption,
                            ascending: defaultSortAscendingOrder) { (result) in
                                switch result {
                                case .success(let ticketsResponse): completion(.success(ticketsResponse.tickets))
                                case .failure(let error): completion(.failure(error))
                                }
        }
    }
}
