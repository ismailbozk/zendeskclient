//
//  TicketListBuilderTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class TicketListBuilderTests: XCTestCase {

    func testBuilder() {
        let viewController = TicketListBuilder().buildTicketListModule()
        XCTAssertTrue(viewController is TicketListViewController)
        XCTAssertTrue(viewController is TicketListViewProtocol)
        let ticketListViewController = viewController as! TicketListViewController
        XCTAssertNotNil(ticketListViewController.eventHandler)
        let eventHandler = ticketListViewController.eventHandler!
        XCTAssertTrue(eventHandler is DefaultTicketListPresenter)
    }
}
