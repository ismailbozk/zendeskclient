//
//  AlamofireNetworking.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import Alamofire

/// Alamofire Networking Manager class
class AlamofireNetworking: NetworkingManager {
    /// Performs given network request and calls completion closure when it is done.
    ///
    /// - Parameters:
    ///   - request: Network request
    ///   - completion: completion closure to notify caller object once it is done along with Response Data or Error.
    func perform(request: URLRequest, _ completion: @escaping (Result<Data>) -> Void) {
        Alamofire.request(request)
            .validate()
            .responseData { response in
                switch response.result {
                case .success(let data):  completion(.success(data))
                case .failure(let error): completion(.failure(error))
                }
        }
    }
}
