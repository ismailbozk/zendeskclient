//
//  TicketListResponseTests.swift
//  ZendeskClientTests
//


import XCTest
@testable import ZendeskClient

class TicketListResponseTests: XCTestCase {
    func testTicketParsing() {
        let ticketsData = try! JSONEncoder().encode(tickets())
        let decodedTickets = try! JSONDecoder().decode([Ticket].self, from: ticketsData)
        
        XCTAssertTrue(decodedTickets.count == 2)
        XCTAssertTrue(decodedTickets.first!.id == 0)
        XCTAssertTrue(decodedTickets.last!.id == 1)
        XCTAssertTrue(decodedTickets.first!.subject == "Something happened")
        XCTAssertTrue(decodedTickets.last!.subject == "Something happened again")
        XCTAssertTrue(decodedTickets.first!.description == "something happened and I don't know what to do")
        XCTAssertTrue(decodedTickets.last!.description == "something happened and I don't know what to do again")
        XCTAssertTrue(decodedTickets.first!.status == .pending)
        XCTAssertTrue(decodedTickets.last!.status == .new)
    }
    
    func testTicketResponseParsing() {
        let responseData = try! JSONEncoder().encode(TicketListResponse(tickets: tickets()))
        let decodedResponse = try! JSONDecoder().decode(TicketListResponse.self, from: responseData)
        
        let decodedTickets = decodedResponse.tickets
        
        XCTAssertTrue(decodedTickets.count == 2)
        XCTAssertTrue(decodedTickets.first!.id == 0)
        XCTAssertTrue(decodedTickets.last!.id == 1)
        XCTAssertTrue(decodedTickets.first!.subject == "Something happened")
        XCTAssertTrue(decodedTickets.last!.subject == "Something happened again")
        XCTAssertTrue(decodedTickets.first!.description == "something happened and I don't know what to do")
        XCTAssertTrue(decodedTickets.last!.description == "something happened and I don't know what to do again")
        XCTAssertTrue(decodedTickets.first!.status == .pending)
        XCTAssertTrue(decodedTickets.last!.status == .new)

    }
    
    private func tickets() -> [Ticket] {
        return [
            Ticket(id: 0, subject: "Something happened", description: "something happened and I don't know what to do", status: .pending),
            Ticket(id: 1, subject: "Something happened again", description: "something happened and I don't know what to do again", status: .new),
        ]
    }
}
