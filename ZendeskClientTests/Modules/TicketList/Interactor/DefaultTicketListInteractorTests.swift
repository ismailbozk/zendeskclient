//
//  DefaultTicketListInteractorTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class DefaultTicketListInteractorTests: XCTestCase {
    private var serviceMock: TicketListServiceMock!
    private var interactor: DefaultTicketListInteractor!
    
    override func setUp() {
        serviceMock = TicketListServiceMock()
        interactor = DefaultTicketListInteractor(ticketService: serviceMock)
    }

    func testFetchTickets() {
        interactor.fetchTickets { (result) in
            switch result {
            case .success(let tickets):
                XCTAssertTrue(tickets.count == 2)
                XCTAssertTrue(tickets.first!.id == 0)
                XCTAssertTrue(tickets.last!.id == 1)
            case .failure(_): XCTAssert(false)
            }
        }
        
        XCTAssertTrue(serviceMock.calledFunctions.contains("fetchTicket(userName:password:sortBy:ascending:_:)"))
        XCTAssertTrue(serviceMock.defaultSortAscendingOrder == true)
        XCTAssertTrue(serviceMock.defaultSortingOption == .id)
        XCTAssertTrue(serviceMock.agentUserName == "acooke+techtest@zendesk.com")
        XCTAssertTrue(serviceMock.agenPassword == "mobile")
    }
}

private final class TicketListServiceMock: BaseMock, TicketService {
    var defaultSortingOption: TicketSortByOption!
    var defaultSortAscendingOrder: Bool!
    var agentUserName: String!
    var agenPassword: String!

    func fetchTicket(userName: String, password: String, sortBy: TicketSortByOption?, ascending: Bool?, _ completion: @escaping (Result<TicketListResponse>) -> Void) {
        defaultSortingOption = sortBy
        defaultSortAscendingOrder = ascending
        agentUserName = userName
        agenPassword = password

        calledFunctions.append(#function)
        
        completion(.success(ticketListResponseMock()))
    }
    
    func ticketListResponseMock() -> TicketListResponse {
        let ticket1 = Ticket(id: 0, subject: "Something is broken", description: "I have tried turning it off and on", status: .pending)
        let ticket2 = Ticket(id: 1, subject: "I need help", description: "I don't know what to do", status: .new)
        return TicketListResponse(tickets: [ticket1, ticket2])
    }
}
