//
//  DefaultTicketListPresenterTests.swift
//  ZendeskClientTests
//

import XCTest
@testable import ZendeskClient

class DefaultTicketListPresenterTests: XCTestCase {
    private var viewMock: TicketListViewMock!
    private var interactorMock: TicketListInteractorMock!
    private var mapperMock: TicketListMapperMock!
    
    private var presenter: DefaultTicketListPresenter!
    
    override func setUp() {
        viewMock = TicketListViewMock()
        interactorMock = TicketListInteractorMock()
        mapperMock = TicketListMapperMock()
        
        presenter = DefaultTicketListPresenter(view: viewMock, interactor: interactorMock, mapper: mapperMock)
    }

    func testViewDidAppear() {
        interactorMock.expectedResponse = .success([Ticket(id: 666, subject: "", description: "", status: .new)])
        
        presenter.viewDidAppear()
        
        XCTAssertTrue(viewMock.calledFunctions.contains("startLoading()"))
        XCTAssertTrue(viewMock.calledFunctions.contains("dismissLoading()"))
        XCTAssertTrue(interactorMock.calledFunctions.contains("fetchTickets"))
        XCTAssertTrue(viewMock.calledFunctions.contains("display(tickets:)"))
        XCTAssertFalse(viewMock.calledFunctions.contains("display(error:)"))

        XCTAssertTrue(viewMock.displayedTickets!.count == 1)
        XCTAssertTrue(viewMock.displayedTickets!.first!.id == "666")
    }
    
    func testViewDidAppearErrorCase() {
        interactorMock.expectedResponse = nil
        
        presenter.retryTapped()
        
        XCTAssertTrue(viewMock.calledFunctions.contains("startLoading()"))
        XCTAssertTrue(viewMock.calledFunctions.contains("dismissLoading()"))
        XCTAssertTrue(interactorMock.calledFunctions.contains("fetchTickets"))
        XCTAssertFalse(viewMock.calledFunctions.contains("display(tickets:)"))
        XCTAssertTrue(viewMock.calledFunctions.contains("display(error:)"))
        
        XCTAssertNil(viewMock.displayedTickets)
    }
    
    func testRetry() {
        interactorMock.expectedResponse = .success([Ticket(id: 400, subject: "", description: "", status: .new)])
        
        presenter.viewDidAppear()
        
        XCTAssertTrue(viewMock.calledFunctions.contains("startLoading()"))
        XCTAssertTrue(viewMock.calledFunctions.contains("dismissLoading()"))
        XCTAssertTrue(interactorMock.calledFunctions.contains("fetchTickets"))
        XCTAssertTrue(viewMock.calledFunctions.contains("display(tickets:)"))
        XCTAssertFalse(viewMock.calledFunctions.contains("display(error:)"))
        
        XCTAssertTrue(viewMock.displayedTickets!.count == 1)
        XCTAssertTrue(viewMock.displayedTickets!.first!.id == "400")

    }
}


private final class TicketListViewMock: BaseMock, TicketListViewProtocol {
    var displayedTickets: [TicketDisplay]?
    
    func display(tickets: [TicketDisplay]) {
        displayedTickets = tickets
        calledFunctions.append(#function)
    }
    
    func startLoading() {
        calledFunctions.append(#function)
    }
    
    func dismissLoading() {
        calledFunctions.append(#function)
    }
    
    func display(error: String) {
        calledFunctions.append(#function)
    }
}

private final class TicketListInteractorMock: BaseMock, TicketListInteractor {
    var expectedResponse: Result<[Ticket]>?
    func fetchTickets(_ completion: @escaping (Result<[Ticket]>) -> Void) {
        calledFunctions.append(#function)
        
        if let response = expectedResponse {
            completion(response)
        }
        else {
            completion(.failure(NSError(domain: "somedomain", code: 404, userInfo: nil)))
        }
    }
}

private final class TicketListMapperMock: TicketListMapper {
    override func mapTicketsToTicketDisplay(tickets: [Ticket]) -> [TicketDisplay] {
        return tickets.map {
            TicketDisplay(id: String($0.id),
                          subject: String($0.subject),
                          status: $0.status.rawValue.capitalized,
                          statusColor: UIColor.red,
                          description: String($0.description))
        }
    }
}
