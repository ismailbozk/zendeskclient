//
//  TicketListResponse.swift
//  ZendeskClient
//
//  https://app.quicktype.io/
//

import Foundation

struct TicketListResponse: Codable {
    let tickets: [Ticket]
    
    enum CodingKeys: String, CodingKey {
        case tickets
    }
}

struct Ticket: Codable {
    let id: Int
    let subject: String
    let description: String
    let status: Status
    
    enum CodingKeys: String, CodingKey {
        case id
        case subject
        case description
        case status
    }
}

enum Status: String, Codable {
    case new = "new"
    case pending = "pending"
    case statusOpen = "open"
}

