//
//  NSErrorExtension.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import UIKit

extension NSError {
    /// A generic invalid JSON Error
    ///
    /// - Returns: an NSError along with generic invalid json error desciption.
    class func invalidJSONError() -> NSError {
        let userInfo = [NSLocalizedDescriptionKey: NSLocalizedString("INVALID_JSON_ERROR", comment: "Invalid json error message")]
        return NSError(domain: "com.ibozkurt.json.parsing", code: -666, userInfo: userInfo)
    }
}
