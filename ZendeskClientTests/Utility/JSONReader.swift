//
//  JSONReader.swift
//  ZendeskClientTests
//

import XCTest

final class JSONReader {
    class func jsonData(fileName name:String) throws -> Data {
        guard let path = Bundle(for: JSONReader.self)
            .path(forResource: name, ofType: "json") else {
                XCTFail("Could not find file \(name).json")
                throw NSError(domain: "JSONHelper", code: -3242342343, userInfo: nil)
        }
        return try Data(contentsOf: URL(fileURLWithPath: path), options: .dataReadingMapped)
    }
}
