//
//  TicketListViewController.swift
//  ZendeskClient
//
//  Created by Ismail Bozkurt on 20/10/2018.
//

import UIKit

class TicketListViewController: UIViewController {
    var eventHandler: TicketListEventHandler?
    
    private var tickets: [TicketDisplay] = []
    
    private lazy var alertController: UIAlertController = { return self.buildErrorAlertViewController() }()
    private lazy var retryBarButtonItem = UIBarButtonItem(barButtonSystemItem: .refresh, target: self, action: #selector(retryTapped))
    @IBOutlet private weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet private weak var tableView: UITableView! {
        didSet {
            tableView.accessibilityIdentifier = "TicketListTableView"
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = NSLocalizedString("TICKETS", comment: "Tickets page title")
        navigationItem.rightBarButtonItem = retryBarButtonItem
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        eventHandler?.viewDidAppear()
    }
    
    // MARK: - Actions
    @objc private func retryTapped() {
        eventHandler?.retryTapped()
    }
    
    // MARK: - Privates
    private func buildErrorAlertViewController() -> UIAlertController {
        let title = NSLocalizedString("FETCH_TICKETS_FAILED_TITLE", comment: "failed to load ticket alert title")
        let alertController = UIAlertController(title: title, message: "", preferredStyle: .alert)
        let cancelTitle = NSLocalizedString("CANCEL_BUTTON_TITLE", comment: "Cancel alert button title")
        let action = UIAlertAction(title: cancelTitle, style: .cancel, handler: nil)
        alertController.addAction(action)
        
        let retryTitle = NSLocalizedString("TRY_AGAIN_BUTTON_TITLE", comment: "Try Again button title")
        let retryAction = UIAlertAction(title: retryTitle, style: .default, handler:{ [weak self] (action: UIAlertAction) in
            self?.eventHandler?.retryTapped()
        })
        alertController.addAction(retryAction)
        
        return alertController
    }
}

// MARK: - UITableView DataSource
extension TicketListViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return tickets.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: String(describing: TicketCell.self)) as? TicketCell else {
            fatalError("TicketCell couldn't load!")
        }
        cell.updateContent(with: tickets[indexPath.row])
        return cell
    }
}

extension TicketListViewController: TicketListViewProtocol {
    func display(tickets: [TicketDisplay]) {
        self.tickets = tickets
        tableView.scrollRectToVisible(CGRect(x: 0.0, y: 0.0, width: 1.0, height: 1.0), animated: false)
        tableView.reloadData()
    }
    
    func display(error: String) {
        self.alertController.message = error
        self.present(self.alertController, animated: true, completion: nil)
    }
    
    func startLoading() {
        tableView.isHidden = true
        activityIndicator.startAnimating()
        retryBarButtonItem.isEnabled = false
    }
    
    func dismissLoading() {
        tableView.isHidden = false
        activityIndicator.stopAnimating()
        retryBarButtonItem.isEnabled = true
    }    
}
