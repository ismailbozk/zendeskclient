//
//  AlamofireNetworkingTests.swift
//  AlamofireNetworkingTests
//

import XCTest
@testable import ZendeskClient

class AlamofireNetworkingTests: XCTestCase {
    var alamofireNetworking: AlamofireNetworking!
    let timeOutInterval: TimeInterval = 60.0
    
    override func setUp() {
        alamofireNetworking = AlamofireNetworking()
    }

    func testNetworkCall() {
        let expectation = self.expectation(description: "HTTP GET call")

        let url = URL(string: "https://mxtechtest.zendesk.com/api/v2/views/39551161/tickets.json")!
        var request = URLRequest(url: url, cachePolicy: .reloadIgnoringLocalAndRemoteCacheData, timeoutInterval: timeOutInterval)
        request.addBasicAuthCredentials(userName: "acooke+techtest@zendesk.com", password: "mobile")
        alamofireNetworking.perform(request: request) { (result) in
            expectation.fulfill()
            switch result {
            case .success(_): XCTAssert(true)
            case .failure(_): XCTAssert(false)
            }
        }
        
        self.wait(for: [expectation], timeout: timeOutInterval+1)
    }
}
